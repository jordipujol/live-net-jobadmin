#!/bin/sh

cd "$(dirname $0)"

_shasum() {
	find . -not -path '*/.*' \
	-not -path "${buildstamp}" \
	-not -path "${0}" \
	-type f | \
		sha256sum
}

buildstamp="debian/debhelper-build-stamp"

changed=""
if [ ! -e "${buildstamp}" ] || \
[ -n "${changed:="$(find . -not -path '*/.*' \
	-not -path "${buildstamp}" \
	-not -path "${0}" \
	-type f \
	-cnewer "${buildstamp}")"}" ] || \
[ "$(_shasum)" != "$(cat "${buildstamp}")" ]; then
	printf '%s\n' "Changed files:" "${changed:-"all"}" ""
	! debuild -tc || \
		_shasum > "${buildstamp}"
else
	echo "Nothing to do" >&2
fi
