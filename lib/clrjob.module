#!/bin/bash

#  Jobs management application for the Linux OS
#  $Revision: 1.95 $
#
#  Copyright (C) 2008-2024 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This file is part of the LneT Jobadmin package,
#   http://livenet.ddns.net .
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 1, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#  Please report bugs to <jordipujolp AT gmail DOT com>.
#
#
#************************************************************************

# Clear Job history

_clrjob_usage="Clear Job history

clrjob -j|--job 'JOBNAME'

Clear Job history of executions data"

_clrjob_options="JA_JOB pr@
JA_USR pr@
JA_EXECS pr@
JA_KEEP pr@"

_clrjob_by_exec() {
	# global rc attrs job_execlist
	local job_exec="${1}"
	local f found ja_execs

	found=""
	while read f; do
		[ -e "${f}" ] || \
			continue
		rm -f "${f}"
		found="y"
	done < <(find "${JA_JOB_DIR}" -maxdepth 1 -type f \
	-name "job-${job_exec}[^[:digit:]]*")
	if [ -n "${found}" ]; then
		UsrNotify "Removing job execution '${JA_JOB}.${JA_USR}.${job_exec}'."
		if ja_execs="$(sed -nre "\|\b("${job_exec}")\b[^[:blank:]]+[[:blank:]]*| \
		{s|||p;q0};q1" <<< "${job_execlist}")"; then
			job_execlist="${ja_execs}"
			_set_option "${attrs}" "JA_EXECLIST" "${ja_execs}"
		fi
	else
		rc=1
		UsrNotify "Error: job execution '${JA_JOB}.${JA_USR}.${job_exec}' doesn't exist."
	fi
}

_clrjob() {
	local fd1 job_exec
	local rc attrs job_execlist # global

	if ! _test_rw "${JA_JOB_DIR}"; then
		UsrNotify "Error: Not authorized to delete job '${JA_JOB}.${JA_USR}'."
		return 1
	fi

	( if ! flock --exclusive --timeout 1 ${fd1}; then
		UsrNotify "Error: job '${JA_JOB}.${JA_USR}' is locked."
		return 1
	fi
	rc=0
	if [ "${JA_EXECS}" = "all" ]; then
		( set +o noglob 
		while IFS= read -r job_cmd; do
			rm -f "${job_cmd%".cmd"}"[.-]*
		done < <( ls -1t "${JA_JOB_DIR}job-"*.cmd | \
		tail -qn "+$((JA_KEEP+1))" ) )

		attrs="${JA_JOB_DIR}job.attrs"
		if [ ${JA_KEEP} = 0 ]; then
			_set_option "${attrs}" "JA_EXECLIST" ""
			_set_option "${attrs}" "JA_EXEC" ""
		else
			_set_option "${attrs}" "JA_EXECLIST" \
				"$(_read_option "JA_EXECLIST" "${attrs}" | \
				tr -s '[:blank:]' '\n' | \
				tail -qn ${JA_KEEP} | \
				tr -s '\n' '\t')"
		fi
	elif [ "${JA_KEEP}" != 0 ]; then
		UsrNotify "Error: Can't remove some exec pids while keeping a fixed number."
		return 1
	else
		attrs="${JA_JOB_DIR}job.attrs"
		if job_execlist="$(_read_option "JA_EXECLIST" "${attrs}")"; then
			for job_exec in $(tr -s ',' ' ' <<< "${JA_EXECS}"); do
				_clrjob_by_exec "${job_exec}"
			done
		else
			UsrNotify "Warning: no executions exist for '${JA_JOB}.${JA_USR}'."
		fi
	fi

	return $rc
	) {fd1}< "$(_flock_lckfile "${JA_JOB_DIR}")"
} # clrjob
