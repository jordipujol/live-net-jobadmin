#!/bin/bash

#  Jobs management application for the Linux OS
#  $Revision: 1.95 $
#
#  Copyright (C) 2008-2024 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This file is part of the LneT Jobadmin package,
#   http://livenet.ddns.net .
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 1, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#  Please report bugs to <jordipujolp AT gmail DOT com>.
#
#
#************************************************************************

# display job schedule properties

_dspjobscde_usage="Display Job Schedule

dspjobscde -j|--job 'job'

- display job schedule properties"

_dspjobscde_options="JA_JOB pr@
JA_ENTRYNBR pr@
JA_USR"

_dspjobscde() {
	local splf d h fd1

	if ! test -r "${JA_JOB_DIR}"; then
		UsrNotify "Error: Not authorized to manage job '${JA_JOB}.${JA_USR}'."
		return 1
	fi

	[ -n "${JA_EXEC}" ] || \
		_job_exist_exec || :

	(
	UsrNotify "Displaying job '${JA_JOB}.${JA_USR}${JA_EXEC:+".${JA_EXEC}"}'."
	# TODO: formatted listing of the job
	printf '%s\n\n' "Job '${JA_JOB}.${JA_USR}'"
		_printvars "${JA_JOB_DIR}job.attrs"
		printf '\nCommand:\n'
		awk 'NR == 2 && $0 != "# jobadmin script" {print "Custom script"; exit}
NR == 3' "${JA_JOB_DIR}job.cmd"
		printf '\n%s\n' "Spool files:"
		while read splf; do
			[ -e "${splf}" ] || continue
			printf '%s %s\n' "$(basename "${splf}")" \
				"$(_datetime --date=@$(stat -c '%Y' "${splf}"))"
		done < <( ( set +o noglob
			(ls -1c "${JA_JOB_DIR}"*.splfa 2> /dev/null) ) )
		if [ -n "${JA_EXEC}" ]; then
			printf '\n%s\n\n' "Job execution '${JA_JOB}.${JA_USR}.${JA_EXEC}'"
			_printvars "${JA_JOB_DIR}job-${JA_EXEC}.attrs"
			printf '\nCommand:\n'
			awk 'NR == 2 && $0 != "# jobadmin script" {print "Custom script"; exit}
NR == 3' "${JA_JOB_DIR}job-${JA_EXEC}.cmd"
		fi
	) 2>&1 | pager
} # dspjobscde
